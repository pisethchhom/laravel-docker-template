#!/bin/sh

cd /var/www

php artisan cache:clear
php artisan route:cache

chgrp -R www-data storage bootstrap/cache
chmod -R ug+rwx storage bootstrap/cache

/usr/bin/supervisord -c /etc/supervisord.conf